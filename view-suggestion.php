<?php
session_name('hydridweb');
session_start();
include 'includes/connect.php';
include 'includes/config.php';

if (!isset($_SESSION['discord_id'])) {
  session_unset();
  header('Location: '.$url['auth'].'');
  exit();
}

include 'includes/loggedIn.php';

$page['name'] = 'Viewing Suggestion';
if (isset($_GET["sid"])) {
  $suggestion_id = strip_tags($_GET['sid']);
  $sql = "SELECT * FROM suggestions WHERE id= ?";
  $stmt = $pdo->prepare($sql);
  $stmt->execute([$suggestion_id]);
  $suggestionInfo = $stmt->fetch(PDO::FETCH_ASSOC);

  $sql2 = "SELECT * FROM users WHERE uid= ?";
  $stmt2 = $pdo->prepare($sql2);
  $stmt2->execute([$suggestionInfo['user_id']]);
  $authorInfo = $stmt2->fetch(PDO::FETCH_ASSOC);

  if ($suggestionInfo === false) {
    header('Location: '.$url['suggestions'].'?error=not-found');
    exit();
  }
} else {
  header('Location: '.$url['suggestions'].'?error=not-found');
  exit();
}

if (isset($_POST['upvoteBtn'])) {
  //Check if the suggestion is actually opened
  if ($suggestionInfo['status'] == 'DENIED' || $suggestionInfo['status'] == 'ACCEPTED') {
    header('Location: '.$url['suggestions'].'?error=cant-vote');
    exit();
  }

  //Check if the person has already voted
  $sql = "SELECT count(*) FROM `suggestion_upvotes` WHERE sid = ? AND uid = ?";
  $stmt = $pdo->prepare($sql);
  $stmt->execute([$suggestionInfo['id'], $user['id']]);
  $checkIfAlreadyVoted = $stmt->fetchColumn();

  if ($checkIfAlreadyVoted != '0') {
    header('Location: '.$url['suggestions'].'?error=already-voted');
    exit();
  } else {
    //If the suggestion already has X upvotes, mark it as highly voted
    $sql3 = "SELECT count(*) FROM `suggestion_upvotes` WHERE sid = ?";
    $stmt3 = $pdo->prepare($sql3);
    $stmt3->execute([$suggestionInfo['id']]);
    $getTotalVotes = $stmt3->fetchColumn();

    if ($getTotalVotes >= '20') {
      if ($suggestionInfo['status'] !== "HIGHLY VOTED") {
        $sql4 = "UPDATE suggestions SET status=? WHERE id=?";
        $pdo->prepare($sql4)->execute(['HIGHLY VOTED', $suggestionInfo['id']]);

        discordAlert('**Suggestion #'.$suggestionInfo['id'].' has been detected with High Upvotes! Staff will notice this suggestion easier now.** :fire:');
      }
    }

    $sql2 = "INSERT INTO suggestion_upvotes (sid, uid) VALUES (?,?)";
    $pdo->prepare($sql2)->execute([$suggestionInfo['id'], $user['id']]);


    discordAlert('**<@'.$_SESSION['discord_id'].'> has upvoted Suggestion #'.$suggestionInfo['id'].'**');

    header('Location: '.$url['suggestions'].'?action=suggestion-created');
    exit();
  }
} elseif (isset($_POST['acceptBtn'])) {
  $sql = "UPDATE suggestions SET status=? WHERE id=?";
  $pdo->prepare($sql)->execute(['ACCEPTED', $suggestionInfo['id']]);

  discordAlert('**<@'.$_SESSION['discord_id'].'> has accepted Suggestion #'.$suggestionInfo['id'].'! This will be implemented soon by our developers.**');

  header('Location: '.$url['suggestions'].'?action=suggestion-accepted');
  exit();
} elseif (isset($_POST['denyBtn'])) {
  $sql = "UPDATE suggestions SET status=? WHERE id=?";
  $pdo->prepare($sql)->execute(['DENIED', $suggestionInfo['id']]);

  discordAlert('**<@'.$_SESSION['discord_id'].'> has denied Suggestion #'.$suggestionInfo['id'].'**');

  header('Location: '.$url['suggestions'].'?action=suggestion-denied');
  exit();
} elseif (isset($_POST['newCommentBtn'])) {
  $comment_msg    = !empty($_POST['comment']) ? trim($_POST['comment']) : null;
  $comment_msg    = strip_tags($comment_msg);

  $sql = "INSERT INTO suggestion_comments (comment, uid, sid, timestamp) VALUES (?,?,?,?)";
  $pdo->prepare($sql)->execute([$comment_msg, $user['id'], $suggestionInfo['id'], $datetime]);

  discordAlert('**New Comment Added by <@'.$_SESSION['discord_id'].'> on Suggestion #'.$suggestionInfo['id'].'**
**Comment:** '.$comment_msg.'');

  header('Location: '.$url['suggestions'].'?action=comment-created');
  exit();
} elseif (isset($_POST['deleteBtn'])) {
  $sql = "DELETE from suggestions WHERE id=?";
  $pdo->prepare($sql)->execute([$suggestionInfo['id']]);

  header('Location: '.$url['suggestions'].'?action=suggestion-deleted');
  exit();
}
?>
<!DOCTYPE html>
<html>
 <?php include 'includes/page/head.php'; ?>
 <?php include 'includes/page/nav-bar.php'; ?>
  <div class="wrapper">
     <div class="container-fluid">
        <div class="row">
           <div class="col-sm-12">
              <div class="page-title-box">
                 <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                       <li class="breadcrumb-item">
                          <a href="#"><?php echo $settings['name']; ?></a>
                       </li>
                       <li class="breadcrumb-item active"><?php echo $page['name']; ?></li>
                    </ol>
                 </div>
                 <h4 class="page-title"><?php echo $page['name']; ?></h4>
              </div>
           </div>
        </div>
        <!-- PAGE CONTENT START -->
          <?php print($displayMessage); ?>
          <?php if ($suggestionInfo['status'] == 'DENIED'): ?>
            <div class="alert alert-danger" role="alert">This suggestion has already been denied, and can no longer be commented on or voted for.</div>
          <?php elseif($suggestionInfo['status'] == 'ACCEPTED'): ?>
            <div class="alert alert-success" role="alert">This suggestion has already been accepted, and can no longer be commented on or voted for.</div>
          <?php elseif($suggestionInfo['status'] == 'HIGHLY VOTED'): ?>
            <div class="alert alert-warning" role="alert">This is a popular suggestion! This is likely to be noticed by Staff soon.</div>
          <?php endif; ?>
          <div class="row">
            <!-- Left Side -->
            <div class="col-9">
              <div class="card-box">
                <h4 class="header-title">Viewing Suggestion: <?php echo $suggestionInfo['name']; ?></h4><hr />
                <div class="form-group">
                  <textarea class="form-control" rows="12" readonly="true"><?php echo $suggestionInfo['description']; ?></textarea>
                </div>
                <form method="POST">
                  <div class="row">
                    <div class="col-3">
                      <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#createCommentModal"><i class="fas fa-plus"></i> New Comment</button>
                    </div>
                    <div class="col-3">
                      <button type="submit" name="upvoteBtn" class="btn btn-success btn-block"><i class="fas fa-arrow-circle-up"></i> Upvote</button>
                    </div>
                  </div>
                </form>
              </div>
              <?php
              $sql = "SELECT count(*) FROM `suggestion_comments` WHERE sid = ?";
              $result = $pdo->prepare($sql);
              $result->execute([$suggestionInfo['id']]);
              $totalComments = $result->fetchColumn();
              ?>
              <?php if ($totalComments == '0'): ?>
                <div class="alert alert-info" role="alert">We couldn't find any comments for this suggestion! Be the first to make one.</div>
              <?php else: ?>
                <?php
                $sql             = "SELECT * FROM suggestion_comments WHERE sid = ?";
                $stmt            = $pdo->prepare($sql);
                $stmt->execute([$suggestionInfo['id']]);
                $listComments  = $stmt->fetchAll(PDO::FETCH_ASSOC);

                foreach ($listComments as $comment) {

                  $sql_user = "SELECT * FROM users WHERE uid = ?";
                  $stmt_user = $pdo->prepare($sql_user);
                  $stmt_user->execute([$comment['uid']]);
                  $userData = $stmt_user->fetch(PDO::FETCH_ASSOC);
                ?>
                <div class="card-box">
                  <h4 class="header-title">Comment - @<?php echo $userData['name'];
                  if ($userData['usergroup'] == 'Helper') {
                    echo ' <span class="badge badge-warning">Community Helper</span>';
                  } elseif ($userData['usergroup'] == 'Staff') {
                    echo ' <span class="badge badge-dark">Community Staff</span>';
                  } elseif ($userData['usergroup'] == 'Super Admin') {
                    echo ' <span class="badge badge-danger">Super Admin</span>';
                  }
                  ?> - <?php echo $comment['timestamp'] ?></h4><hr />
                  <div class="form-group">
                    <textarea class="form-control" rows="12" readonly="true"><?php echo $comment['comment']; ?></textarea>
                  </div>
                </div>
                <?php } ?>
              <?php endif; ?>
            </div>

            <!-- Right Side -->
            <div class="col-3">
              <div class="card-box">
                <h4 class="header-title">Info</h4><hr />
                <h6><strong>Author:</strong> <?php echo $authorInfo['name']; ?></h6>
                <?php
                $sql = "SELECT count(*) FROM `suggestion_upvotes` WHERE sid = ?";
                $result = $pdo->prepare($sql);
                $result->execute([$suggestionInfo['id']]);
                $totalUpvotes = $result->fetchColumn();
                ?>
                <h6><strong>Upvotes:</strong> <?php echo $totalUpvotes; ?></h6>
              </div>

              <?php if ($user['group'] == 'Staff' || $user['group'] == 'Super Admin'): ?>
                <div class="card-box">
                  <h4 class="header-title">Staff Actions</h4><hr />
                  <form method="POST">
                    <div class="row">
                      <div class="col-6">
                        <button type="submit" name="acceptBtn" class="btn btn-success btn-block">Accept</button>
                      </div>
                      <div class="col-6">
                        <button type="submit" name="denyBtn" class="btn btn-danger btn-block">Deny</button>
                      </div>
                    </div>
                  </form>
                </div>
              <?php endif; ?>
            </div>
          </div>
        <!-- PAGE CONTENT END -->
        <!-- Page Modals -->
        <!-- Create Comment Modal -->
        <div class="modal fade" id="createCommentModal" tabindex="-1" role="dialog" aria-labelledby="createCommentModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="createCommentModalLabel">New Comment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form method="POST">
              <div class="modal-body">
                <div class="form-group">
                  <div class="row">
                    <div class="col-12">
                      <label for="comment">Comment</label>
                      <textarea class="form-control" name="comment" id="comment" rows="6" placeholder="Comment...."></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" name="newCommentBtn">Submit</button>
              </div>
              </form>
            </div>
          </div>
        </div>
        <!-- End Modals -->
     </div>
  </div>
  <?php include 'includes/page/footer.php'; ?>
</body>
</html>
