<?php
session_name('hydridweb');
session_start();
include 'includes/connect.php';
include 'includes/config.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (isset($_GET["error"])) {
	echo 'Auth Error.';
} elseif (isset($_GET["banned"])) {
    echo 'Account Banned.';
} elseif (isset($_GET["code"])) {
	$redirect_uri = "https://DOMAIN.COM/auth.php";
	$token_request = "https://discordapp.com/api/oauth2/token";
	$token = curl_init();
	curl_setopt_array($token, array(
		CURLOPT_URL => $token_request,
		CURLOPT_POST => 1,
		CURLOPT_POSTFIELDS => array(
			"grant_type" => "authorization_code",
			"client_id" => "CLIENT_ID",
			"client_secret" => "CLIENT_SECRET",
			"redirect_uri" => $redirect_uri,
			"code" => $_GET["code"]
		)
	));
	curl_setopt($token, CURLOPT_RETURNTRANSFER, true);
	$resp = json_decode(curl_exec($token));
	curl_close($token);
	if (isset($resp->access_token)) {
		$access_token = $resp->access_token;
		$info_request = "https://discordapp.com/api/users/@me";
		$info = curl_init();
		curl_setopt_array($info, array(
			CURLOPT_URL => $info_request,
			CURLOPT_HTTPHEADER => array(
				"Authorization: Bearer {$access_token}"
			) ,
			CURLOPT_RETURNTRANSFER => true
		));
		$user = json_decode(curl_exec($info));
        curl_close($info);

        $_SESSION['username']   = $user->username . '#' . $user->discriminator;
        $_SESSION['discord_id']   = $user->id;
        $_SESSION['access_token']   = $access_token;
        $_SESSION['logged_in'] = time();
        $_SESSION['user_avatar'] = $user->avatar;

        //Check if the discord id is already in the system, else add it
        $sql = "SELECT * FROM users WHERE discord_id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$_SESSION['discord_id']]);
        $userRow = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($userRow === false) {
            $sql2 = "INSERT INTO users (name, discord_id) VALUES (?,?)";
            $pdo->prepare($sql2)->execute([$_SESSION['username'], $_SESSION['discord_id']]);
        }

        header('Location: '.$url['suggestions'].'');
        exit();
		// echo "<h1>Hello, {$user->username}#{$user->discriminator}.</h1><br /><h2>{$user->id}</h2><br /><img src='https://discordapp.com/api/v6/users/{$user->id}/avatars/{$user->avatar}.jpg' /><br /><br />Dashboard Token: {$access_token}";
	}
	else {
		header("Location: REDIRECT_URL");
    exit();
	}
}
else {
    header("Location: REDIRECT_URL");
    exit();
}

?>
