# IMPORTANT
THIS PROJECT IS NOT IN ACTIVE DEVELOPMENT. THIS IS ONE OF MY OLDER PROJECTS THAT HAS NOW BEEN RELEASED FOR FREE. NO UPDATES WILL BE PERFORMED UNLESS A MAJOR BUG IS DISCOVERED OR A COMMUNITY PR.

# BAS
BAS is a web-based Bug and Suggestion system built for gaming communities although it can be utilized for any project. 

# Support
Please use GitLab's "issues" system for any support. Support will not be provided on Discord.

# System Requirements
- PHP 7 or Higher
- Linux or Windows OS
- PDO 

# Discord
https://discord.io/HydridSystems
