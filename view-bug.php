<?php
session_name('hydridweb');
session_start();
include 'includes/connect.php';
include 'includes/config.php';

if (!isset($_SESSION['discord_id'])) {
  session_unset();
  header('Location: '.$url['auth'].'');
  exit();
}

include 'includes/loggedIn.php';

$page['name'] = 'Viewing Bug';
if (isset($_GET["bid"])) {
  $bug_id = strip_tags($_GET['bid']);
  $sql = "SELECT * FROM bugs WHERE id= ?";
  $stmt = $pdo->prepare($sql);
  $stmt->execute([$bug_id]);
  $bugInfo = $stmt->fetch(PDO::FETCH_ASSOC);

  $sql2 = "SELECT * FROM users WHERE uid= ?";
  $stmt2 = $pdo->prepare($sql2);
  $stmt2->execute([$bugInfo['user_id']]);
  $authorInfo = $stmt2->fetch(PDO::FETCH_ASSOC);

  if ($bugInfo === false) {
    header('Location: '.$url['bugs'].'?error=not-found');
    exit();
  }
} else {
  header('Location: '.$url['bugs'].'?error=not-found');
  exit();
}

if (isset($_POST['upvoteBtn'])) {
  //Check if the suggestion is actually opened
  if ($bugInfo['status'] === 'FIXED' || $bugInfo['status'] === 'CLOSED') {
    header('Location: '.$url['bugs'].'?error=cant-vote');
    exit();
  }

  //Check if the person has already voted
  $sql = "SELECT count(*) FROM `bug_upvotes` WHERE bid = ? AND uid = ?";
  $stmt = $pdo->prepare($sql);
  $stmt->execute([$bugInfo['id'], $user['id']]);
  $checkIfAlreadyVoted = $stmt->fetchColumn();

  if ($checkIfAlreadyVoted != '0') {
    header('Location: '.$url['bugs'].'?error=already-voted');
    exit();
  } else {
    //If the suggestion already has X upvotes, mark it as highly voted
    $sql3 = "SELECT count(*) FROM `bug_upvotes` WHERE bid = ?";
    $stmt3 = $pdo->prepare($sql3);
    $stmt3->execute([$bugInfo['id']]);
    $getTotalVotes = $stmt3->fetchColumn();

    if ($bugInfo['status'] !== "FLAGGED") {
      if ($getTotalVotes >= '20') {
        if ($bugInfo['status'] !== "HIGHLY VOTED") {
          $sql4 = "UPDATE bugs SET status=? WHERE id=?";
          $pdo->prepare($sql4)->execute(['HIGHLY VOTED', $bugInfo['id']]);

          discordAlert('**Bug #'.$bugInfo['id'].' has been detected with High Upvotes! Developers will notice this suggestion easier now.** :fire:');
        }
      }
    }

    $sql2 = "INSERT INTO bug_upvotes (bid, uid) VALUES (?,?)";
    $pdo->prepare($sql2)->execute([$bugInfo['id'], $user['id']]);


    discordAlert('**<@'.$_SESSION['discord_id'].'> has upvoted Bug #'.$bugInfo['id'].'**');

    header('Location: '.$url['bugs'].'?action=bug-upvoted');
    exit();
  }
} elseif (isset($_POST['newCommentBtn'])) {
  $comment_msg    = !empty($_POST['comment']) ? trim($_POST['comment']) : null;
  $comment_msg    = strip_tags($comment_msg);

  $sql = "INSERT INTO bug_comments (comment, uid, bid, timestamp) VALUES (?,?,?,?)";
  $pdo->prepare($sql)->execute([$comment_msg, $user['id'], $bugInfo['id'], $datetime]);

  discordAlert('**New Comment Added by <@'.$_SESSION['discord_id'].'> on Bug #'.$bugInfo['id'].'**
**Comment:** '.$comment_msg.'');

  header('Location: '.$url['bugs'].'?action=comment-created');
  exit();
} elseif (isset($_POST['fixedBtn'])) {
  $sql = "UPDATE bugs SET status=? WHERE id=?";
  $pdo->prepare($sql)->execute(['FIXED', $bugInfo['id']]);

  discordAlert('**<@'.$_SESSION['discord_id'].'> has marked Bug #'.$bugInfo['id'].' as Fixed. If you still notice this issue, create a new bug report.**');

  header('Location: '.$url['bugs'].'');
  exit();
} elseif (isset($_POST['flagBtn'])) {
  $sql = "UPDATE bugs SET status=? WHERE id=?";
  $pdo->prepare($sql)->execute(['FLAGGED', $bugInfo['id']]);

  discordAlert('**<@'.$_SESSION['discord_id'].'> has flagged Bug #'.$bugInfo['id'].'. This will now be a priority for developers.**');

  header('Location: '.$url['bugs'].'');
  exit();
} elseif (isset($_POST['closeBtn'])) {
  $sql = "UPDATE bugs SET status=? WHERE id=?";
  $pdo->prepare($sql)->execute(['CLOSED', $bugInfo['id']]);

  discordAlert('**<@'.$_SESSION['discord_id'].'> has closed Bug #'.$bugInfo['id'].'. If you still notice this issue, create a new bug report.**');

  header('Location: '.$url['bugs'].'');
  exit();
}
?>
<!DOCTYPE html>
<html>
 <?php include 'includes/page/head.php'; ?>
 <?php include 'includes/page/nav-bar.php'; ?>
  <div class="wrapper">
     <div class="container-fluid">
        <div class="row">
           <div class="col-sm-12">
              <div class="page-title-box">
                 <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                       <li class="breadcrumb-item">
                          <a href="#"><?php echo $settings['name']; ?></a>
                       </li>
                       <li class="breadcrumb-item active"><?php echo $page['name']; ?></li>
                    </ol>
                 </div>
                 <h4 class="page-title"><?php echo $page['name']; ?></h4>
              </div>
           </div>
        </div>
        <!-- PAGE CONTENT START -->
          <?php print($displayMessage); ?>
          <?php if ($bugInfo['status'] == 'FIXED'): ?>
            <div class="alert alert-info" role="alert">This bug report has been fixed by developers. If you are still having this issue, Please create a new bug report.</div>
          <?php elseif($bugInfo['status'] == 'CLOSED'): ?>
            <div class="alert alert-danger" role="alert">This bug report has been closed.</div>
          <?php elseif($bugInfo['status'] == 'HIGHLY VOTED'): ?>
            <div class="alert alert-warning" role="alert">This bug has been upvoted a high amount of times. This will allow our developers to fix this issue quicker.</div>
          <?php elseif($bugInfo['status'] == 'FLAGGED'): ?>
            <div class="alert alert-danger" role="alert">This bug has been flagged by a Helper as CRITICAL.</div>
          <?php endif; ?>
          <div class="row">
            <!-- Left Side -->
            <div class="col-9">
              <div class="card-box">
                <h4 class="header-title">Viewing Suggestion: <?php echo $bugInfo['name']; ?></h4><hr />
                <div class="form-group">
                  <textarea class="form-control" rows="12" readonly="true"><?php echo $bugInfo['description']; ?></textarea>
                </div>
                <form method="POST">
                  <div class="row">
                    <div class="col-3">
                      <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#createCommentModal"><i class="fas fa-plus"></i> New Comment</button>
                    </div>
                    <div class="col-3">
                      <button type="submit" name="upvoteBtn" class="btn btn-success btn-block"><i class="fas fa-arrow-circle-up"></i> Upvote</button>
                    </div>
                  </div>
                </form>
              </div>
              <?php
              $sql = "SELECT count(*) FROM `bug_comments` WHERE bid = ?";
              $result = $pdo->prepare($sql);
              $result->execute([$bugInfo['id']]);
              $totalComments = $result->fetchColumn();
              ?>
              <?php if ($totalComments == '0'): ?>
                <div class="alert alert-info" role="alert">We couldn't find any comments for this suggestion! Be the first to make one.</div>
              <?php else: ?>
                <?php
                $sql             = "SELECT * FROM bug_comments WHERE bid = ?";
                $stmt            = $pdo->prepare($sql);
                $stmt->execute([$bugInfo['id']]);
                $listComments  = $stmt->fetchAll(PDO::FETCH_ASSOC);

                foreach ($listComments as $comment) {

                  $sql_user = "SELECT * FROM users WHERE uid = ?";
                  $stmt_user = $pdo->prepare($sql_user);
                  $stmt_user->execute([$comment['uid']]);
                  $userData = $stmt_user->fetch(PDO::FETCH_ASSOC);
                ?>
                <div class="card-box">
                  <h4 class="header-title">Comment - @<?php echo $userData['name'];
                  if ($userData['usergroup'] == 'Helper') {
                    echo ' <span class="badge badge-warning">Community Helper</span>';
                  } elseif ($userData['usergroup'] == 'Staff') {
                    echo ' <span class="badge badge-dark">Community Staff</span>';
                  } elseif ($userData['usergroup'] == 'Super Admin') {
                    echo ' <span class="badge badge-danger">Super Admin</span>';
                  }
                  ?> - <?php echo $comment['timestamp'] ?></h4><hr />
                  <div class="form-group">
                    <textarea class="form-control" rows="12" readonly="true"><?php echo $comment['comment']; ?></textarea>
                  </div>
                </div>
                <?php } ?>
              <?php endif; ?>
            </div>

            <!-- Right Side -->
            <div class="col-3">
              <div class="card-box">
                <h4 class="header-title">Info</h4><hr />
                <h6><strong>Author:</strong> <?php echo $authorInfo['name']; ?></h6>
                <?php
                $sql = "SELECT count(*) FROM `bug_upvotes` WHERE bid = ?";
                $result = $pdo->prepare($sql);
                $result->execute([$bugInfo['id']]);
                $totalUpvotes = $result->fetchColumn();
                ?>
                <h6><strong>Upvotes:</strong> <?php echo $totalUpvotes; ?></h6>
              </div>

              <?php if ($user['group'] == 'Staff' || $user['group'] == 'Super Admin'): ?>
                <div class="card-box">
                  <h4 class="header-title">Staff Actions</h4><hr />
                  <form method="POST">
                    <div class="row">
                      <div class="col-6">
                        <button type="submit" name="fixedBtn" class="btn btn-info btn-block">Fixed</button>
                      </div>
                      <div class="col-6">
                        <button type="submit" name="closeBtn" class="btn btn-danger btn-block">Close</button>
                      </div>
                    </div>
                  </form>
                </div>

                <div class="card-box">
                  <h4 class="header-title">Helper Actions</h4><hr />
                  <form method="POST">
                    <div class="row">
                      <div class="col-12">
                        <button type="submit" name="flagBtn" class="btn btn-danger btn-block">Flag as Critical</button>
                      </div>
                    </div>
                  </form>
                </div>
              <?php endif; ?>
            </div>
          </div>
        <!-- PAGE CONTENT END -->
        <!-- Page Modals -->
        <!-- Create Comment Modal -->
        <div class="modal fade" id="createCommentModal" tabindex="-1" role="dialog" aria-labelledby="createCommentModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="createCommentModalLabel">New Comment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form method="POST">
              <div class="modal-body">
                <div class="form-group">
                  <div class="row">
                    <div class="col-12">
                      <label for="comment">Comment</label>
                      <textarea class="form-control" name="comment" id="comment" rows="6" placeholder="Comment...."></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" name="newCommentBtn">Submit</button>
              </div>
              </form>
            </div>
          </div>
        </div>
        <!-- End Modals -->
     </div>
  </div>
  <?php include 'includes/page/footer.php'; ?>
</body>
</html>
