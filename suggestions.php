<?php
session_name('hydridweb');
session_start();
include 'includes/connect.php';
include 'includes/config.php';

if (!isset($_SESSION['discord_id'])) {
  session_unset();
  header('Location: '.$url['auth'].'');
  exit();
}

include 'includes/loggedIn.php';

$page['name'] = 'Suggestions';

if (isset($_POST['newSuggestionBtn'])) {
  $suggestionName    = !empty($_POST['suggestionName']) ? trim($_POST['suggestionName']) : null;
  $suggestionName    = strip_tags($suggestionName);

  $suggestionDesc    = !empty($_POST['suggestionDesc']) ? trim($_POST['suggestionDesc']) : null;
  $suggestionDesc    = strip_tags($suggestionDesc);

  $sql = "INSERT INTO suggestions (name, description, user_id, timestamp) VALUES (?,?,?,?)";
  $pdo->prepare($sql)->execute([$suggestionName, $suggestionDesc, $user['id'], $datetime]);

  discordAlert('**New Suggestion Added by <@'.$_SESSION['discord_id'].'>**
**Topic:** '.$suggestionName.'
You can view and upvote this suggestion on the web panel!');

  header('Location: '.$url['suggestions'].'?action=suggestion-created');
  exit();
}

if (isset($_GET['action']) && strip_tags($_GET['action']) === 'suggestion-created') {
  $displayMessage = '<div class="alert alert-success" role="alert">New Suggestion Added</div>';
} elseif (isset($_GET['error']) && strip_tags($_GET['error']) === 'not-found') {
  $displayMessage = '<div class="alert alert-danger" role="alert">That suggestion can not be found! It may have been deleted.</div>';
} elseif (isset($_GET['error']) && strip_tags($_GET['error']) === 'already-voted') {
  $displayMessage = '<div class="alert alert-danger" role="alert">You can only vote for a suggestion once!</div>';
} elseif (isset($_GET['action']) && strip_tags($_GET['action']) === 'suggestion-accepted') {
  $displayMessage = '<div class="alert alert-success" role="alert">You have accepted a suggestion!</div>';
} elseif (isset($_GET['error']) && strip_tags($_GET['error']) === 'cant-vote') {
  $displayMessage = '<div class="alert alert-danger" role="alert">You can not vote for this suggestion. It may be closed already.</div>';
} elseif (isset($_GET['action']) && strip_tags($_GET['action']) === 'suggestion-denied') {
  $displayMessage = '<div class="alert alert-danger" role="alert">You have denied a suggestion!</div>';
} elseif (isset($_GET['action']) && strip_tags($_GET['action']) === 'comment-created') {
  $displayMessage = '<div class="alert alert-success" role="alert">New Comment Added</div>';
} elseif (isset($_GET['action']) && strip_tags($_GET['action']) === 'suggestion-deleted') {
  $displayMessage = '<div class="alert alert-danger" role="alert">Suggestion Deleted</div>';
}
?>
<!DOCTYPE html>
<html>
 <?php include 'includes/page/head.php'; ?>
 <?php include 'includes/page/nav-bar.php'; ?>
  <div class="wrapper">
     <div class="container-fluid">
        <div class="row">
           <div class="col-sm-12">
              <div class="page-title-box">
                 <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                       <li class="breadcrumb-item">
                          <a href="#"><?php echo $settings['name']; ?></a>
                       </li>
                       <li class="breadcrumb-item active"><?php echo $page['name']; ?></li>
                    </ol>
                 </div>
                 <h4 class="page-title"><?php echo $page['name']; ?></h4>
              </div>
           </div>
        </div>
        <!-- PAGE CONTENT START -->
          <?php print($displayMessage); ?>
          <div class="row">
            <div class="col-12">
              <div class="card-box">
                <h4 class="header-title">All Suggestions
                  <div class="float-right">
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createSuggestionModal">
                      New Suggestion
                    </button>
                  </div>
                </h4><br />
                <table id="suggestionsTable" class="table table-borderless">
                  <thead>
                    <tr>
                      <th scope="col">Suggestion ID</th>
                      <th scope="col">Name</th>
                      <th scope="col">Status</th>
                      <th scope="col">Upvotes</th>
                      <th scope="col">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $sql2             = "SELECT * FROM suggestions";
                    $stmt2            = $pdo->prepare($sql2);
                    $stmt2->execute();
                    $listSuggestions  = $stmt2->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($listSuggestions as $suggestion) {
                    ?>
                    <tr>
                      <th scope="row"><?php echo $suggestion['id']; ?></th>
                      <td><?php echo truncate_string($suggestion['name'], 50, ' ...'); ?></td>
                      <td>
                      <?php if($suggestion['status'] == "OPEN") {
                        echo '<span class="badge badge-success">OPEN</span>';
                      }elseif ($suggestion['status'] == "HIGHLY VOTED") {
                        echo '<span class="badge badge-warning">HIGHLY VOTED</span>';
                      }elseif ($suggestion['status'] == "DENIED") {
                        echo '<span class="badge badge-danger">DENIED</span>';
                      }elseif ($suggestion['status'] == "ACCEPTED") {
                        echo '<span class="badge badge-info">ACCEPTED</span>';
                      } ?>
                      </td>
                      <?php
                      $sql3 = "SELECT count(*) FROM `suggestion_upvotes` WHERE sid = ?";
                      $result3 = $pdo->prepare($sql3);
                      $result3->execute([$suggestion['id']]);
                      $totalUpvotes = $result3->fetchColumn();
                      ?>
                      <td><?php echo $totalUpvotes; ?></td>
                      <td><a href="<?php echo $url['view-suggestion']; ?>?sid=<?php echo $suggestion['id']; ?>" class="btn btn-primary btn-block">View</a></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        <!-- PAGE CONTENT END -->

        <!-- Page Modals -->
        <!-- Create Suggestion Modal -->
        <div class="modal fade" id="createSuggestionModal" tabindex="-1" role="dialog" aria-labelledby="createSuggestionModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="createSuggestionModalLabel">Create Suggestion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form method="POST">
              <div class="modal-body">
                <div class="form-group">
                  <div class="row">
                    <div class="col-12">
                      <label for="suggestionName">Suggestion Name</label>
                      <input type="text" class="form-control" name="suggestionName" id="suggestionName" placeholder="Suggestion Name" required>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-12">
                      <label for="suggestionDesc">Suggestion Description (For pictures, please use imgur.)</label>
                      <textarea class="form-control" name="suggestionDesc" id="suggestionDesc" rows="6" placeholder="Please describe your suggestion." required></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" name="newSuggestionBtn">Submit</button>
              </div>
              </form>
            </div>
          </div>
        </div>
        <!-- End Modals -->
     </div>
  </div>
  <?php include 'includes/page/footer.php'; ?>
</body>
</html>
