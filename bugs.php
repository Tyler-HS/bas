<?php
session_name('hydridweb');
session_start();
include 'includes/connect.php';
include 'includes/config.php';

if (!isset($_SESSION['discord_id'])) {
  session_unset();
  header('Location: '.$url['auth'].'');
  exit();
}

include 'includes/loggedIn.php';

$page['name'] = 'Bugs';

if (isset($_POST['newBugBtn'])) {
  $bugName    = !empty($_POST['bugName']) ? trim($_POST['bugName']) : null;
  $bugName    = strip_tags($bugName);

  $bugDesc    = !empty($_POST['bugDesc']) ? trim($_POST['bugDesc']) : null;
  $bugDesc    = strip_tags($bugDesc);

  $sql = "INSERT INTO bugs (name, description, user_id, timestamp) VALUES (?,?,?,?)";
  $pdo->prepare($sql)->execute([$bugName, $bugDesc, $user['id'], $datetime]);

  discordAlert('**New Bug Reported by <@'.$_SESSION['discord_id'].'>**
**Topic:** '.$bugName.'
You can view and upvote this bug on the web panel!');

  header('Location: '.$url['bugs'].'?action=bug-created');
  exit();
}

if (isset($_GET['action']) && strip_tags($_GET['action']) === 'bug-created') {
  $displayMessage = '<div class="alert alert-success" role="alert">New Bug Reported</div>';
} elseif (isset($_GET['error']) && strip_tags($_GET['error']) === 'not-found') {
  $displayMessage = '<div class="alert alert-danger" role="alert">That bug can not be found! It may have been deleted.</div>';
} elseif (isset($_GET['error']) && strip_tags($_GET['error']) === 'already-voted') {
  $displayMessage = '<div class="alert alert-danger" role="alert">You can only vote once!</div>';
} elseif (isset($_GET['action']) && strip_tags($_GET['action']) === 'bug-accepted') {
  $displayMessage = '<div class="alert alert-success" role="alert">You have accepted a suggestion!</div>';
} elseif (isset($_GET['error']) && strip_tags($_GET['error']) === 'cant-vote') {
  $displayMessage = '<div class="alert alert-danger" role="alert">You can not vote for this bug. It may be closed already.</div>';
} elseif (isset($_GET['action']) && strip_tags($_GET['action']) === 'bug-denied') {
  $displayMessage = '<div class="alert alert-danger" role="alert">You have denied a suggestion!</div>';
} elseif (isset($_GET['action']) && strip_tags($_GET['action']) === 'comment-created') {
  $displayMessage = '<div class="alert alert-success" role="alert">New Comment Added</div>';
} elseif (isset($_GET['action']) && strip_tags($_GET['action']) === 'bug-upvoted') {
  $displayMessage = '<div class="alert alert-warning" role="alert">Bug Upvoted</div>';
}
?>
<!DOCTYPE html>
<html>
 <?php include 'includes/page/head.php'; ?>
 <?php include 'includes/page/nav-bar.php'; ?>
  <div class="wrapper">
     <div class="container-fluid">
        <div class="row">
           <div class="col-sm-12">
              <div class="page-title-box">
                 <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                       <li class="breadcrumb-item">
                          <a href="#"><?php echo $settings['name']; ?></a>
                       </li>
                       <li class="breadcrumb-item active"><?php echo $page['name']; ?></li>
                    </ol>
                 </div>
                 <h4 class="page-title"><?php echo $page['name']; ?></h4>
              </div>
           </div>
        </div>
        <!-- PAGE CONTENT START -->
          <?php print($displayMessage); ?>
          <div class="row">
            <div class="col-12">
              <div class="card-box">
                <h4 class="header-title">All Bugs
                  <div class="float-right">
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createBugModal">
                      New Bug
                    </button>
                  </div>
                </h4><br />
                <table id="bugTable" class="table table-borderless">
                  <thead>
                    <tr>
                      <th scope="col">Bug ID</th>
                      <th scope="col">Name</th>
                      <th scope="col">Status</th>
                      <th scope="col">Upvotes</th>
                      <th scope="col">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $sql             = "SELECT * FROM bugs";
                    $stmt            = $pdo->prepare($sql);
                    $stmt->execute();
                    $listBugs  = $stmt->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($listBugs as $bug) {
                    ?>
                    <tr>
                      <th scope="row"><?php echo $bug['id']; ?></th>
                      <td><?php echo truncate_string($bug['name'], 50, ' ...'); ?></td>
                      <td>
                      <?php if($bug['status'] == "OPEN") {
                        echo '<span class="badge badge-success">OPEN</span>';
                      }elseif ($bug['status'] == "HIGHLY VOTED") {
                        echo '<span class="badge badge-warning">HIGHLY VOTED</span>';
                      }elseif ($bug['status'] == "FIXED") {
                        echo '<span class="badge badge-info">FIXED</span>';
                      }elseif ($bug['status'] == "CLOSED") {
                        echo '<span class="badge badge-danger">CLOSED</span>';
                      }elseif ($bug['status'] == "FLAGGED") {
                        echo '<span class="badge badge-danger">FLAGGED</span>';
                      } ?>
                      </td>
                      <?php
                      $sql3 = "SELECT count(*) FROM `bug_upvotes` WHERE bid = ?";
                      $result3 = $pdo->prepare($sql3);
                      $result3->execute([$bug['id']]);
                      $totalUpvotes = $result3->fetchColumn();
                      ?>
                      <td><?php echo $totalUpvotes; ?></td>
                      <td><a href="<?php echo $url['view-bug']; ?>?bid=<?php echo $bug['id']; ?>" class="btn btn-primary btn-block">View</a></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        <!-- PAGE CONTENT END -->

        <!-- Page Modals -->
        <!-- Create Bug Modal -->
        <div class="modal fade" id="createBugModal" tabindex="-1" role="dialog" aria-labelledby="createBugModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="createBugModalLabel">Create Bug</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form method="POST">
              <div class="modal-body">
                <div class="form-group">
                  <div class="row">
                    <div class="col-12">
                      <label for="bugName">Bug Name</label>
                      <input type="text" class="form-control" name="bugName" id="bugName" placeholder="Bug Name" required>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-12">
                      <label for="bugDesc">Bug Description (For pictures, please use imgur.)</label>
                      <textarea class="form-control" name="bugDesc" id="bugDesc" rows="6" placeholder="Please describe your bug. Be as descriptive as possible and make sure you include how developers can reproduce this issue." required></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" name="newBugBtn">Submit</button>
              </div>
              </form>
            </div>
          </div>
        </div>
        <!-- End Modals -->
     </div>
  </div>
  <?php include 'includes/page/footer.php'; ?>
</body>
</html>
