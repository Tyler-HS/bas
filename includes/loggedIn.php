<?php
if (isset($_SESSION['discord_id'])) {
    $sql_user = "SELECT * FROM users WHERE discord_id = ?";
    $stmt_user = $pdo->prepare($sql_user);
    $stmt_user->execute([$_SESSION['discord_id']]);
    $userRow = $stmt_user->fetch(PDO::FETCH_ASSOC);

    if ($userRow === false) {
        $sql2 = "INSERT INTO users (name, discord_id) VALUES (?,?)";
        $pdo->prepare($sql2)->execute([$_SESSION['username'], $_SESSION['discord_id']]);
    } else {
        $user['id'] = $userRow['uid'];
        $user['name'] = $userRow['name'];
        $user['group'] = $userRow['usergroup'];

        $perms['suggestion_close'] = false;
        $perms['suggestion_reopen'] = false;
        $perms['bug_close'] = false;
        $perms['bug_reopen'] = false;
        $perms['bug_flag'] = false;

        switch($user['group']) {
            case "Helper":
            $perms['bug_flag'] = true;
            break;

            case "Staff":
            $perms['suggestion_close'] = true;
            $perms['suggestion_reopen'] = true;
            $perms['bug_close'] = true;
            $perms['bug_reopen'] = true;
            $perms['bug_flag'] = true;
            break;
        }
    }
}
?>
