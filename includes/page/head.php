<head>
   <meta charset="utf-8" />
   <title><?php echo $page['name']; ?> - Hydrid Web Panel</title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
   <meta content="Hydrid Web Panel" name="description" /> <!-- YOU MAY CHANGE THIS -->
   <meta content="Hydrid Systems" name="author" /> <!-- YOU MAY NOT CHANGE THIS -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge" />
   <!-- App favicon -->
   <link rel="shortcut icon" href="assets/images/favicon.ico">
   <!-- Thrid Party Css -->
   <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/v/bs4/dt-1.10.18/b-1.5.4/b-html5-1.5.4/datatables.min.css"/>
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

   <!-- App css -->
   <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="../../assets/css/icons.css" rel="stylesheet" type="text/css" />
   <link href="../../assets/css/style.css" rel="stylesheet" type="text/css" />
   <script src="../../assets/js/modernizr.min.js"></script>
</head>
<body>
   <!-- Navigation Bar-->
   <header id="topnav">
      <div class="topbar-main">
         <div class="container-fluid">
            <!-- Logo container-->
            <div class="logo">
               <!-- Text Logo -->
               <a href="<?php echo $url['suggestions']; ?>" class="logo">
               <span class="logo-small"><i class="mdi mdi-bug"></i></span>
               <span class="logo-large"><i class="mdi mdi-bug"></i> Hydrid</span>
               </a>
            </div>
            <!-- End Logo container-->
            <div class="menu-extras topbar-custom">
               <ul class="list-unstyled topbar-right-menu float-right mb-0">
                  <li class="menu-item">
                     <!-- Mobile menu toggle-->
                     <a class="navbar-toggle nav-link">
                        <div class="lines">
                           <span></span>
                           <span></span>
                           <span></span>
                        </div>
                     </a>
                     <!-- End mobile menu toggle-->
                  </li>
                  <li class="dropdown notification-list">
                     <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                     <img src="https://cdn.discordapp.com/avatars/<?php echo $_SESSION['discord_id']; ?>/<?php echo $_SESSION['user_avatar']; ?>.png" alt="user" class="rounded-circle">
                     <span class="ml-1 pro-user-name">@<?php echo $user['name']; ?>
                     </span>
                     </a>
                  </li>
               </ul>
            </div>
            <!-- end menu-extras -->
            <div class="clearfix"></div>
         </div>
         <!-- end container -->
      </div>
