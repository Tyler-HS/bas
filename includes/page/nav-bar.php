<div class="navbar-custom">
   <div class="container-fluid">
      <div id="navigation">
         <ul class="navigation-menu">
            <li class="has-submenu">
               <a href="<?php echo $url['suggestions']; ?>"><i class="fi-cloud"></i>Suggestions</a>
            </li>
            <li class="has-submenu">
               <a href="<?php echo $url['bugs']; ?>"><i class="fi-help"></i>Bugs</a>
            </li>
         </ul>
      </div>
   </div>
</div>
</header>
