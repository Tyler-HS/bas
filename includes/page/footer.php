<footer class="footer">
   <div class="container">
      <div class="row">
         <div class="col-12 text-center">
            <?php echo $year; ?> © <a href="https://discord.io/HydridSystems">Hydrid Systems</a> - All Rights Reserved.
         </div>
      </div>
   </div>
</footer>
<!-- End Footer -->
<!-- jQuery  -->
<script src="../../assets/js/jquery.min.js"></script>
<script src="../../assets/js/popper.min.js"></script>
<script src="../../assets/js/bootstrap.min.js"></script>
<script src="../../assets/js/waves.js"></script>
<script src="../../assets/js/jquery.slimscroll.js"></script>
<!-- Third Party JS -->
<script type="text/javascript" src="//cdn.datatables.net/v/bs4/dt-1.10.18/b-1.5.4/b-html5-1.5.4/datatables.min.js"></script>
<!-- App js -->
<script src="../../assets/js/jquery.core.js"></script>
<script src="../../assets/js/jquery.app.js"></script>

<script type="text/javascript">
$(document).ready( function () {
  $('#suggestionsTable').DataTable( {
    "order": [[ 3, 'desc' ]],
    "pagingType": "numbers",
    "bLengthChange": false
  } );
  $('#bugTable').DataTable( {
    "order": [[ 3, 'desc' ]],
    "pagingType": "numbers",
    "bLengthChange": false
  } );
  if ( window.history.replaceState ) {
      window.history.replaceState( null, null, window.location.href );
  }
});
</script>
