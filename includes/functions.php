<?php
function discordAlert($message) {
    global $discord_webhook;
    //=======================================================================
    // Create new webhook in your Discord channel settings and copy&paste URL
    //=======================================================================
    $webhookurl = $discord_webhook;
    //=======================================================================
    // Compose message. You can use Markdown
    //=======================================================================
    $json_data = array(
        'content' => "$message"
    );
    $make_json = json_encode($json_data);
    $ch = curl_init($webhookurl);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $make_json);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);

    return $response;
}

// Log Function
function logAction($action, $user) {
    global $pdo;
    global $time;
    global $us_date;

    $sql_log = "INSERT INTO logs (action, username, timestamp) VALUES (:action, :username, :timestamp)";
    $stmt_log = $pdo->prepare($sql_log);
    $stmt_log->bindValue(':action', $action);
    $stmt_log->bindValue(':username', $user);
    $stmt_log->bindValue(':timestamp', $us_date . ' ' . $time);
    $result_log = $stmt_log->execute();
}

function truncate_string($string, $maxlength, $extension) {

    // Set the replacement for the "string break" in the wordwrap function
    $cutmarker = "**cut_here**";

    // Checking if the given string is longer than $maxlength
    if (strlen($string) > $maxlength) {

        // Using wordwrap() to set the cutmarker
        // NOTE: wordwrap (PHP 4 >= 4.0.2, PHP 5)
        $string = wordwrap($string, $maxlength, $cutmarker);

        // Exploding the string at the cutmarker, set by wordwrap()
        $string = explode($cutmarker, $string);

        // Adding $extension to the first value of the array $string, returned by explode()
        $string = $string[0] . $extension;
    }

    // returning $string
    return $string;

}

function phpAlert($msg) {
    echo '<script type="text/javascript">alert("' . $msg . '")</script>';
}

function validateAccount () {
    global $pdo;
    $sql = "SELECT * FROM users WHERE discord_id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$_SESSION['discord_id']]);
    $userRow = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($userRow === false) {
        header("Location: auth.php");
        exit();
    } elseif ($userRow['usergroup'] == "Banned") {
        header("Location: auth.php?banned");
        exit();
    } 
}

?>
