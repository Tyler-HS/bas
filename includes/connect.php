<?php
// MySQL Settings
define("DB_HOST", "localhost");
define("DB_USER", "hydridus_web");
define("DB_PASS", "");
define("DB_NAME", "hydridus_web");

// Do Not Edit Below --- SERIOUSLY DON'T TOUCH THIS STUFF.
$pdoOptions = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_SILENT,
    PDO::ATTR_EMULATE_PREPARES => false
);
try {
    $pdo = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS, $pdoOptions);
}

catch(Exception $e) {
    die('Unable to connect to database.');
}
?>
